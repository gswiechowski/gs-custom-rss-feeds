<?php
/**
 * RSS 0.92 Feed Template for displaying RSS 0.92 Posts feed.
 *
 * @package WordPress
 */
header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
echo '<?xml-stylesheet type="text/css" href="'.plugins_url('gs-custom-rss-feeds/css/rss.css').'"?>'; ?>
<rss version="0.92">
<channel>
    <title><?php bloginfo_rss('name'); wp_title_rss(); ?> - Custom</title>
    <link><?php bloginfo_rss('url') ?></link>
    <description><?php bloginfo_rss('description') ?></description>
    <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
    <docs>http://backend.userland.com/rss092</docs>
    <language><?php bloginfo_rss( 'language' ); ?></language>
    <?php do_action('rss_head'); ?>

<?php while (have_posts()) : the_post(); ?>
    <item>
        <title><?php the_title_rss() ?></title>
        <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
        <author>
                <name><?php the_author() ?></name>
                <?php $author_url = get_the_author_meta('url');
                if (!empty($author_url)) : ?>
                    <uri><?php the_author_meta('url') ?></uri>
                <?php endif;
                ?>
            </author>
        <description><![CDATA[<?php
            if(has_post_thumbnail($post->ID)) {
                /*
                 * ZDJĘCIE ARTYKUŁU
                 * odkomentować linijkę 67
                 */
                echo '<p><a href="'.get_permalink().'">' . get_the_post_thumbnail($post->ID, 'rss-thumb') . '</a></p>';
            }
            echo '<p style="margin: 5px 0 15px 0; padding-bottom: 10px; border-bottom: 1px solid #EDEDED;">';
			$cont = get_the_excerpt();
            $cont = preg_replace(" (\[.*?\])",'',$cont);
            $cont = strip_shortcodes($cont);
            $cont = strip_tags($cont);
            $cont = substr($cont, 0, 150);
            $cont = substr($cont, 0, strripos($cont, " "));
            $cont = trim(preg_replace( '/\s+/', ' ', $cont));
            echo $cont . '...';
            //the_content_rss('');
            echo '</p>';

        ?>]]>
        </description>
        <link><?php the_permalink_rss() ?></link>
        <?php do_action('rss_item'); ?>
    </item>

    <?php $count++; ?>
<?php endwhile; ?>
</channel>
</rss>
