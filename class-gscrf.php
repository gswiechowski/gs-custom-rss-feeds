<?php
/**
 * GS Custom RSS Feeds
 *
 * @package   gscrf
 * @author    Gniewomir Świechowski <gniewomir.swiechowski@gmail.com>
 * @license
 * @link
 * @copyright 2013 Gniewomir Świechowski
 */

/**
 * Plugin class.
 *
 * @package GS Custom RSS Feeds
 * @author  Gniewomir Świechowski (gniewomir.swiechowski@gmail.com)
 */
class gscrf {

    /**
     * Plugin version, used for cache-busting of style and script file references.
     *
     * @since   1.0.0
     *
     * @var     string
     */
    protected $version = '1.4.1';

    /**
     * Unique identifier for your plugin.
     *
     * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
     * match the Text Domain file header in the main plugin file.
     *
     * @since    1.0.0
     *
     * @var      string
     */
    protected $plugin_slug = 'gscrf';

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Slug of the plugin screen.
     *
     * @since    1.0.0
     *
     * @var      string
     */
    protected $plugin_screen_hook_suffix = null;

    /**
     * Initialize the plugin by setting localization, filters, and administration functions.
     *
     * @since     1.0.0
     */
    private function __construct() {

        // Load plugin text domain
        add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

        // Add the options page and menu item.
        add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

        // Add saving method for ajax
        add_action('wp_ajax_gscrf_data_save', array( $this, 'gscrf_save_ajax' ));
        
        //Add image size for rss item
        if ( function_exists( 'add_image_size' ) ) { 
                add_image_size( 'rss-thumb', 450, 0, true );
        }

        // Load admin style sheet and JavaScript.
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

        $this->add_feed();

    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fired when the plugin is activated.
     *
     * @since    1.0.0
     *
     * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
     */
    public static function activate( $network_wide ) {
        // TODO: Define activation functionality here
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }

    /**
     * Fired when the plugin is deactivated.
     *
     * @since    1.0.0
     *
     * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Deactivate" action, false if WPMU is disabled or plugin is deactivated on an individual blog.
     */
    public static function deactivate( $network_wide ) {
        // TODO: Define deactivation functionality here
        global $wp_rewrite;
        $wp_rewrite->flush_rules();
    }

    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function load_plugin_textdomain() {

        $domain = $this->plugin_slug;
        $locale = apply_filters( 'plugin_locale', get_locale(), $domain );

        load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo' );
        load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
    }

    /**
     * Register and enqueue admin-specific style sheet.
     *
     * @since     1.0.0
     *
     * @return    null    Return early if no settings page is registered.
     */
    public function enqueue_admin_styles() {

        if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
            return;
        }

        $screen = get_current_screen();
        if ( $screen->id == $this->plugin_screen_hook_suffix ) {
            wp_enqueue_style( $this->plugin_slug .'-admin-styles', plugins_url( 'css/admin.css', __FILE__ ), array(), $this->version );
        }

    }

    /**
     * Register and enqueue admin-specific JavaScript.
     *
     * @since     1.0.0
     *
     * @return    null    Return early if no settings page is registered.
     */
    public function enqueue_admin_scripts() {

        if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
            return;
        }

        $screen = get_current_screen();
        if ( $screen->id == $this->plugin_screen_hook_suffix ) {
            // jquery ui 
            wp_enqueue_script( 'jquery-ui-draggable' );
            wp_enqueue_script( 'jquery-ui-droppable' );

            // touch lib
            // TODO https://github.com/McPants/jquery.shapeshift/wiki/Getting-Started#dependencies

            // plugin script
            wp_enqueue_script( $this->plugin_slug . '-admin-script', plugins_url( 'js/admin.js', __FILE__ ), array( 'jquery'  ), $this->version );
        }

    }

    /**
     * Handles saving plugin options
     *
     * @since    1.0.0
     */
    function gscrf_save_ajax() {

        check_ajax_referer('gscrf_data_save', 'security');

        $data = $_POST;
        unset($data['security'], $data['action']);

        if(!is_array(get_option('gscrf-options'))) {
            $options = array();
        } else {
            $options = get_option('gscrf-options');
        }

        if(!empty($data)) {
            $diff = array_diff($options, $data);
            $diff2 = array_diff($data, $options);
            $diff = array_merge($diff, $diff2);
        } else {
            $diff = array();
        }

        if (function_exists('w3tc_pgcache_flush') && function_exists('w3tc_varnish_flush')) {
            $customfeedurl = home_url("?feed=customfeed");
            w3tc_pgcache_flush_url($customfeedurl); 
            w3tc_varnish_flush_url($customfeedurl);
        }

        if(!empty($diff)) {
            if(update_option('gscrf-options', $data)) {
                die('1');
            } else {
                die('0');
            }
        } else {
            die('1');
        }
    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     *
     * @since    1.0.0
     */
    public function add_plugin_admin_menu() {

        $this->plugin_screen_hook_suffix = add_menu_page(
            __( 'Newsletter', $this->plugin_slug ),
            __( 'Newsletter', $this->plugin_slug ),
            'publish_posts',
            $this->plugin_slug,
            array( $this, 'display_plugin_admin_page' ),
            '',
            30
        );

    }

    /**
     * Render the settings page for this plugin.
     *
     * @since    1.0.0
     */
    public function display_plugin_admin_page() {
        include_once( 'views/admin.php' );
    }

    /**
     * NOTE:  Actions are points in the execution of a page or process
     *        lifecycle that WordPress fires.
     *
     *        WordPress Actions: http://codex.wordpress.org/Plugin_API#Actions
     *        Action Reference:  http://codex.wordpress.org/Plugin_API/Action_Reference
     *
     * @since    1.0.0
     */
    public function add_feed() {

        /* TODO
         * rewrites
         * http://www.seodenver.com/custom-rss-feed-in-wordpress/
         */

        function gscrf_create_customfeed() {
            $rss_template = WP_PLUGIN_DIR.'/'.str_replace( basename( __FILE__), "", plugin_basename(__FILE__) ) . 'templates/custom-feed-rss.php';
            load_template( $rss_template );
        }
        add_action('do_feed_customfeed', 'gscrf_create_customfeed', 10, 1);


        function gs_gscrf_pre_get_posts($query) {
            if (!is_admin() && $query->is_main_query() && $query->query_vars['feed'] == 'customfeed') {
                $gscrf_options = get_option('gscrf-options');
                $gscrf_include = explode(',', $gscrf_options['selected'] );
                $query->set('post__in', $gscrf_include);
                $query->set('posts_per_page', -1);
                $query->set('post_status', array( 'publish' ));
                $query->set('orderby', 'post__in' );
            }
        }
        add_action('pre_get_posts', 'gs_gscrf_pre_get_posts');
    }

}
