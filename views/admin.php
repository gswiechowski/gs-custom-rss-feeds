<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   GS Custom RSS Feeds
 * @author    Gniewomir Świechowski <gniewomir.swiechowski@gmail.com>
 * @license
 * @link
 * @copyright 2013 Gniewomir Świechowski
 */
?>
<div class="wrap">
    <?php //screen_icon(); ?>
    <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

    <div id="saved">
        <div id="message" class="updated fade"><p><strong><?php _e('Changes saved.','gscrf'); ?></strong></p></div>
    </div>

    <div id="not-saved">
        <div id="message" class="error fade"><p><strong><?php _e('Changes could not be saved.','gscrf'); ?></strong></p></div>
    </div>    

    <?php $options = get_option('gscrf-options'); ?>
    <form action="/" name="gscrf-form" id="gscrf-form">
        <input type="hidden" name="selected" value="<?php echo $options['selected']; ?>" /><br />
        <input type="hidden" name="stack" value='<?php echo $options['stack']; ?>' /><br />
        <input type="hidden" name="action" value="gscrf_data_save" />
        <input type="hidden" name="security" value="<?php echo wp_create_nonce('gscrf_data_save'); ?>" />
        <input type="button" class="gscrf-clear" value="<?php _e('Clear','gscrf'); ?>" />
        <input type="button" class="gscrf-save" value="<?php _e('Confirm','gscrf'); ?>" />
        <input type="submit" disabled value="<?php _e('Publish','gscrf'); ?>" />
        <button type="button" class="gscrf-link" value="<?php bloginfo('url'); ?>/?feed=customfeed"><?php _e('Open','gscrf'); ?></button>
    </form>

    <h3 style="width:432px; float:left;"><?php _e('Custom RSS','gscrf'); ?></h3>
    <h3 style="width:432px; float:left; margin-left: 20px;"><?php _e('Available posts','gscrf'); ?></h3>
    <div id="target" class="container empty" style="clear:left;">
        <?php
            // The Query
            $target_query = new WP_Query( array(
                'posts_per_page'    => -1,
                'post_status'       => array( 'publish', 'future' ),
                'post__in'          => explode(',',$options['selected']),
                'orderby'           => 'post__in'
            ));
            // The Loop
            if ( $target_query->have_posts() ) {
                while ( $target_query->have_posts() ) {
                $target_query->the_post();
                echo '<div class="clearfix" data-postID="'.  get_the_ID() . '" >' .
                        '<span class="gscrf-meta"><span class="gscrf-position"></span>' . get_the_time('H:i / d-m-y '). '</span>' .
                            get_the_post_thumbnail( get_the_ID() , 'thumbnail' ) .
                            '<h4><a target="_blank" href="'.get_permalink().'">' . get_the_title() . '</a></h4>' .
                        '<p>'.get_the_excerpt().'</p>' .
                    '</div>';
                }
            } else {
                // no posts found
            }
            /* Restore original Post Data */
            wp_reset_postdata();
        ?>
    </div>

    <div id="source" class="container">
    <?php

        // Create a new filtering function that will add our where clause to the query
        function gs_filter_where( $where = '' ) {
            // posts in the last 30 days
            $where .= " AND post_date > '" . date('Y-m-d', strtotime('-1 days')) . "'";
            return $where;
        }


        //add_filter( 'posts_where', 'gs_filter_where' );
        // TODO - uncomment in production
        // The Query
        $query = new WP_Query( array(
            'posts_per_page'    => -1,
            'post_status'       => array( 'publish', 'future' ),
            'post__not_in'      => explode(',',$options['selected']),
        ));
        remove_filter( 'posts_where', 'gs_filter_where' );

        // The Loop
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
            $query->the_post();
                echo '<div class="clearfix" data-postID="'.  get_the_ID() .'" >' .
                        '<span class="gscrf-meta"><span class="gscrf-position"></span>' . get_the_time('H:i / d-m-y '). '</span>' .
                            get_the_post_thumbnail( get_the_ID() , 'thumbnail' ) .
                            '<h4><a target="_blank" href="'.get_permalink().'">' . get_the_title() . '</a></h4>' .
                        '<p>'.get_the_excerpt().'</p>' .
                    '</div>';
            }
        } else {
            // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata();
    ?>
    </div>

</div>
