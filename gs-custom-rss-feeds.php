<?php
/*  Copyright 2014  Gniewomir Świechowski  (email : gniewomir.swiechowski@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * @package   gscrf
 * @author    Gniewomir Świechowski <gniewomir.swiechowski@gmail.com>
 * @license   GPL2
 * @link      http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * @copyright Gniewomir Świechowski
 *
 * @wordpress-plugin
 * Plugin Name: GS Custom RSS Feeds
 * Plugin URI:  https://bitbucket.org/gswiechowski/gs-custom-rss-feeds
 * Description: Custom feeds with post selection UI
 * Version:     1.4.1
 * Author:      Gniewomir Świechowski
 * Author URI:  http://cv.enraged.pl
 * Text Domain: gscrf
 * License:     GPL2
 * License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * Domain Path: /lang
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

require_once( plugin_dir_path( __FILE__ ) . 'class-gscrf.php' );

// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
register_activation_hook( __FILE__, array( 'gscrf', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'gscrf', 'deactivate' ) );

gscrf::get_instance();
