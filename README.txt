=== GS Custom RSS Feeds ===
Contributors: Gniewomir Świechowski
Tags: rss
Requires at least: 3.6
Tested up to: 3.6
Stable tag: 1.4.1
License: GPL2
License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Plugin provides simple UI to create custom rss feed, allowing for drag&drop content (posts) to rss feed. 

== Description ==

Plugin provides simple UI to create custom rss feed, allowing for drag&drop content (posts) to rss feed. 

== Installation ==

1. Upload plugin files to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==


== Screenshots ==


== Todo ==

* multiple feeds
* responsive layout for admin part
* browser cache busting for feed preview  

== Changelog ==

= 1.4.1 =

* translation ready

= 1.4 =
* removed preview 
* removed custom ad manager integration
* light code refactoring 
* simplified admin css
* moved to jquery included in WordPress, instead Google hosted JQuery 1.10.1, since WordPress moved to 1.10.2 shapeshift plugin do not require diffrent jQuery version. Plugin needs at least Wordpress 3.6
* GPL2 license

= 1.3.1 =
* preview feed

= 1.2.1 =
* bugfix
* cosmetic changes

= 1.2.0 =
* better Ad's managment
* manual Ad's rotation in feed
* safety checks to avoid human errors

= 1.1.0 =
* simple Ad's managment

= 1.0.0 =
* Initial relase
